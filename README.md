# Continuous Integration assignment 2
Build, Run and test Python Flask app In GitLAB CI.

### Trigger
This workflows actions will run on push or pull requests.

## docker-build - Deploy the latest Docker image.

```
image: docker:latest
```

### Set stage.

```
stage: build
```
### Services

```
services:
- docker:dind
```
For running a docker commands in GitLab, Use the "bind" command for runing Docker-in-Docker. this is the recommended configuration.

### before_script:

I had to install CRUL first for the test. Otherwise the CRUL command is not found.

```
apk add --update curl && rm -rf /var/cache/apk/*
```

### Runing the script:

### Pull Docker - python-flask-docker.

```
- docker pull lvthillo/python-flask-docker
```

### Run Docker - python-flask-docker.

```
- docker run --name my-container -d -p 8080:8080 lvthillo/python-flask-docker
```

### Add Docker File - Deploy App.
```
- docker build -f Dockerfile .
```

### Test if App is running (Curl)
I used curl command that display a web page with the host name an ip address.
I changed the "localhost" to "docker" this is the syntax for localhost in GitLab CI docker.
```
- curl docker:8080
```

### I verify that the ip is the same as the running container ip.
```
- docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' my-container
```
This command display the ip address of the docer that is the same as the app web page shows.

### I verify the host name that is the same as the running container host name.
```
- docker inspect -f '{{ .Config.Hostname }}' my-container
```
This command display the host name of the docer that is the same as the app web page shows.
